﻿DROP DATABASE IF EXISTS m1u2p3;
CREATE DATABASE IF NOT EXISTS m1u2p3;
USE m1u2p3;
CREATE TABLE poblacion
(
  codpob int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY(codpob)
);
CREATE TABLE clientes
(
  codigo int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY(codigo)
);
CREATE TABLE coches
(
  matricula varchar (10),
  PRIMARY KEY(matricula)
);
CREATE TABLE relacion
(
  codcliente int,
  codpob int,
  matricula varchar(10),
  CONSTRAINT fkrelacioncliente
  FOREIGN KEY (codcliente)
  REFERENCES clientes(codigo)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT,
  CONSTRAINT fkrelacionpoblacion
  FOREIGN KEY (codpob)
  REFERENCES poblacion(codpob)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT,
  CONSTRAINT fkrelacioncoches
  FOREIGN KEY (matricula)
  REFERENCES coches(matricula)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
);
INSERT INTO poblacion (codpob, nombre)
  VALUES (1, 'santander'),
         (2,'pielagos');
INSERT INTO clientes (codigo, nombre)
  VALUES (1, 'Pedro'),
         (2, 'pablo'),
         (3, 'juan');
INSERT INTO coches (matricula)
  VALUES ('m1'),
         ('m2'),
         ('m3');
INSERT INTO relacion (codcliente, codpob, matricula)
  VALUES (1, 1, 'm1'),
         (3, 1, 'm3');

SELECT * FROM poblacion p;
SELECT * FROM  clientes c;
SELECT * FROM coches c;
SELECT * FROM  relacion r;